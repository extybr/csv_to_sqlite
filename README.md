## csv to sqlite
---
### [первый вариант (от ChatGPT) | csv -> sqlite]
	используемые модули:
	* csv
	* sqlite3
	используемые файлы:
	* csv_to_sql.py
	команда:
	python csv_to_sql.py
	
### [второй вариант | xls -> csv -> sqlite]
	используемые модули:
	* libreoffice
	* csv_to_sqlite
	используемые файлы:
	* script.sh
	* csv_sqlite.py
	команда:
	./script.sh myfile
---
