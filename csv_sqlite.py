import sqlite3
import csv

def create_connection(db_file):
    """ create a database connection to the SQLite database specified by db_file """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        print(f"Connected to SQLite database {db_file}")
    except sqlite3.Error as e:
        print(e)
    return conn

def create_table(conn):
    """ create a table from the create_table_sql statement """
    create_table_sql = """
    CREATE TABLE IF NOT EXISTS people (
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL,
        age INTEGER
    );
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
        print("Table created successfully")
    except sqlite3.Error as e:
        print(e)

def insert_data_from_csv(conn, csv_file):
    """ insert data from a csv file into the people table """
    try:
        with open(csv_file, 'r') as file:
            csv_reader = csv.reader(file)
            next(csv_reader)  # Skip the header row
            data = [row for row in csv_reader]
        
        sql_insert = """INSERT INTO people (id, name, age) VALUES (?, ?, ?)"""
        conn.executemany(sql_insert, data)
        conn.commit()
        print("Data inserted successfully from CSV")
    except sqlite3.Error as e:
        print(e)
    except FileNotFoundError:
        print(f"The file {csv_file} does not exist")

def main():
    database = "test.db"
    csv_file = "contacts.csv"

    # create a database connection
    conn = create_connection(database)

    # create table
    if conn is not None:
        create_table(conn)

        # insert data from csv
        insert_data_from_csv(conn, csv_file)
        
        # close the connection
        conn.close()
        print("Connection closed")
    else:
        print("Error! Cannot create the database connection.")

if __name__ == '__main__':
    main()

