import sys
import csv_to_sqlite

first_file = sys.argv[1]
options = csv_to_sqlite.CsvOptions(typing_style="full", encoding="utf-8") 
input_files = [first_file]
csv_to_sqlite.write_csv(input_files, "output.sqlite", options)

