#!/bin/sh
# ./sc.sh myfile
file=$1

libreoffice --headless --convert-to csv "${file}.xls"

venv/bin/python csv_to_sql.py "${file}.csv"

